from PIL import Image, ExifTags
from PIL.ExifTags import TAGS, GPSTAGS
import requests
from io import BytesIO
import datetime as dt
from collections import OrderedDict
from glob import glob
from sys import argv

def get_exif_data(image):
    exif_data = {}
    info = image._getexif()
    if info:
        for tag, value in info.items():
            decoded = TAGS.get(tag, tag)
            exif_data[decoded] = value

    return exif_data


def _get_if_exist(data, key):
    if key in data:
        return data[key]

    return None


def _convert_to_degress(value):
    """Helper function to convert the GPS coordinates stored in the EXIF to degress in float format"""
    d0 = value[0][0]
    d1 = value[0][1]
    d = float(d0) / float(d1)

    m0 = value[1][0]
    m1 = value[1][1]
    m = float(m0) / float(m1)

    s0 = value[2][0]
    s1 = value[2][1]
    s = float(s0) / float(s1)

    return d + (m / 60.0) + (s / 3600.0)

def _convert_to_time_string(value):
    h0 = value[0][0]
    h1 = value[0][1]
    h = int(float(h0)/float(h1))

    m0 = value[1][0]
    m1 = value[1][1]
    m = int(float(m0) / float(m1))

    s0 = value[2][0]
    s1 = value[2][1]
    s = int(float(s0) / float(s1))

    return "{}:{}:{}".format(h,m,s)

def get_lat_lon(exif_data):
    """Returns the latitude and longitude, if available, from the provided exif_data (obtained through get_exif_data above)"""
    lat = None
    lon = None

    if "GPSInfo" in exif_data:
        dummy_gps = exif_data["GPSInfo"]
        gps_info = {}
        for t, value in dummy_gps.items():
            sub_decoded = GPSTAGS.get(t, t)
            gps_info[sub_decoded] = value

        gps_latitude = _get_if_exist(gps_info, "GPSLatitude")
        gps_latitude_ref = _get_if_exist(gps_info, 'GPSLatitudeRef')
        gps_longitude = _get_if_exist(gps_info, 'GPSLongitude')
        gps_longitude_ref = _get_if_exist(gps_info, 'GPSLongitudeRef')

        if gps_latitude and gps_latitude_ref and gps_longitude and gps_longitude_ref:
            lat = _convert_to_degress(gps_latitude)
            if gps_latitude_ref != "N":
                lat = 0 - lat

            lon = _convert_to_degress(gps_longitude)
            if gps_longitude_ref != "E":
                lon = 0 - lon

    return lat, lon


def get_image_width(exif_data):
    if "ImageWidth" not in exif_data:
        return None
    image_width = exif_data.get("ImageWidth")
    return image_width


def get_indian_time(exif_data):
    if "DateTimeOriginal" not in exif_data:
        return None
    indian_time = exif_data.get("DateTimeOriginal")
    return indian_time

def get_device_info(exif_data):
    if "Make" and "Model" not in exif_data:
        return None
    device_make = exif_data.get("Make")
    device_model = exif_data.get("Model")

    return device_make + "/" + device_model


def get_gps_datetime(exif_data):
    """Returns the timestamp, if available, from the provided exif_data"""
    if "GPSInfo" not in exif_data:
        return None
    gps_info = exif_data["GPSInfo"]
    date = gps_info.get("GPSDateStamp")
    time = gps_info.get("GPSTimeStamp")
    if not date or not time:
        return None
    time = _convert_to_time_string(time)

    return date, time

def clean_gps_info(exif_data):
    """Return GPS EXIF info in a more convenient format from the provided exif_data"""
    gps_info = exif_data["GPSInfo"]
    cleaned = OrderedDict()
    cleaned["Latitude"], cleaned["Longitude"] = get_lat_lon(exif_data)
    cleaned["Altitude"] = gps_info.get("GPSAltitude")
    cleaned["Speed"] = gps_info.get("GPSSpeed")
    cleaned["SpeedRef"] = gps_info.get("GPSSpeedRef")
    cleaned["Track"] = gps_info.get("GPSTrack")
    cleaned["TrackRef"] = gps_info.get("GPSTrackRef")
    cleaned["TimeStamp"] = get_gps_datetime(exif_data)
    return cleaned

def get_image_info_from_url(url):
    response = requests.get(url)
    image = Image.open(BytesIO(response.content))
    exif_data = get_exif_data(image)
    image_info = {}

    image_info["Latitude"], image_info["Longitude"] = get_lat_lon(exif_data)
    image_info["Indian Time"] = get_indian_time(exif_data)
    image_info["Device"] = get_device_info(exif_data)

    return image_info


################
# Example ######
################
if __name__ == "__main__":
    response = requests.get('https://s3.amazonaws.com/indianrailway/calender.jpg')
    image = Image.open(BytesIO(response.content))
    exif_data = get_exif_data(image)
#    print(exif_data)
    print (get_lat_lon(exif_data))
    print(get_indian_time(exif_data))
#    print(get_gps_datetime(exif_data))
#    print(get_image_width(exif_data))
    print(get_device_info(exif_data))