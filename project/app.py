# flask_s3_uploads/__init__.py

from flask import Flask, render_template, request, redirect

app = Flask(__name__)
app.config.from_object("config")

from helpers import *
from pilimage import *

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/", methods=["POST"])
def upload_file():
    if "user_file" not in request.files:
        return "No user_file key in request.files"

    file = request.files["user_file"]

    if file.filename == "":
        return "Please select a file"

    if file:
        output =upload_file_to_s3(file, S3_BUCKET)
        imageinfo = get_image_info_from_url(output)
        return str(imageinfo)
    else:
        return redirect("/")

if __name__ == "__main__":
    app.run(port=5000)